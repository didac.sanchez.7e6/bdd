﻿using System;
using System.Collections.Generic;
using System.IO;
using Npgsql;

namespace BDD
{
    /*--- Classes ---*/
    public class Pokemon
    {

        /*--- Atrivutos ---*/

        public int PokedexNum { get; set; }
        public string Name { get; set; }
        public int Generation { get; set; }
        public string Type1 { get; set; }
        public string Type2 { get; set; }
        public bool IsLegendary { get; set; }

        /*--- Constructores ---*/
        public Pokemon(int poknum, string name, int gen, string ty1, string ty2, bool leg)
        {
            PokedexNum = poknum;
            Name = name;
            Generation = gen;
            Type1 = ty1;
            Type2 = ty2;
            IsLegendary = leg;
        }
        public Pokemon(int poknum, string name, int gen, string ty1, bool leg)
        {
            PokedexNum = poknum;
            Name = name;
            Generation = gen;
            Type1 = ty1;
            IsLegendary = leg;
        }
        public override string ToString()
        {
            return $"El pokemon {Name} tiene el numero {PokedexNum}, es de {Generation} generacion, tiene le/los tipo/s {Type1} {Type2}, es {IsLegendary} legendario";
        }
    }
    internal class Program
    {
        // Main program.
        static void Main()
        {
            // Creates and checks for the connection.
            NpgsqlConnection con = Connection("pokemons");
            con.Open();
            if (!CheckStateConnection(con)) return;

            // Does actions, then closes the connection.
            List<string> stats = new List<string>() { "abilities", "against_bug", "against_dark", "against_dragon", "against_electric", "against_fairy", "against_fight", "against_fire", "against_flying", "against_ghost", "against_grass", "against_ground", "against_ice", "against_normal", "against_poison", "against_psychic", "against_rock", "against_steel", "against_water", "attack", "base_egg_steps", "base_happiness", "base_total", "capture_rate", "classfication", "defense", "experience_growth", "height_m", "hp", "name", "percentage_male", "pokedex_number", "sp_attack", "sp_defense", "speed", "type1", "type2", "weight_kg", "generation", "is_legendary" };
            string tableName = "pokemon";
            CreateTable(con, tableName, stats);
            LoadFile(con, tableName, stats);
            CretePokemon(con);
            UpadatePokemon(con, "Charmander", "name","type1","fuego");
            DeletePokemon(con);
            SelectPokemon(con, tableName);
            tableName = "Legendarios";
            CreateTable(con, tableName, new List<string> { "pokedex_number", "name", "generation", "type1", "type2" });
            LoadLegendary(con, tableName, new List<string> { "pokedex_number", "name", "generation", "type1", "type2" }, stats);
            SelectPokemon(con, tableName);

            con.Close();
        }

        private static void LoadLegendary(NpgsqlConnection con, string tableName, List<string> stats, List<string> findeIndex)
        {
            // File path.
            string filePath = @"..\..\..\pokemon.csv";

            // Check if the CSV file exists.
            if (!File.Exists(filePath))
            {
                // Message stating CSV file could not be located.
                Console.WriteLine("Could not locate the CSV file.");
                return;
            }

            // Assign the CSV file to a reader object.
            StreamReader reader = new StreamReader(filePath);
            string[] currentRow;
            List<string> datas = new List<string> { };
            string line;

            // Cleans the .CSV file.
            while ((line = reader.ReadLine()) != null)
            {
                line = line.Replace("['", "").Replace("']", "");
                currentRow = line.Split(';');
                if (line.Contains(";name")) continue;
                datas.Add(currentRow[findeIndex.FindIndex(x => x == "pokedex_number")]);
                datas.Add(currentRow[findeIndex.FindIndex(x => x == "name")]);
                datas.Add(currentRow[findeIndex.FindIndex(x => x == "generation")]);
                datas.Add(currentRow[findeIndex.FindIndex(x => x == "type1")]);
                datas.Add(currentRow[findeIndex.FindIndex(x => x == "type2")]);
                if (currentRow[currentRow.Length -1] == "1")InsertDataPokemon(datas.ToArray(), con, tableName, stats);
                datas.Clear();
            }
        }

        // Delete legendary pokemons
        private static void DeletePokemon(NpgsqlConnection con)
        {
            NpgsqlCommand cmd = new NpgsqlCommand("Delete from pokemon where is_legendary = '1'", con);
            cmd.ExecuteNonQuery();
        }


        // Create Pokemons
        public static void CretePokemon(NpgsqlConnection con)
        {
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT pokedex_number,name,generation,type1,type2,is_legendary FROM pokemon", con);
            using NpgsqlDataReader pokemon = cmd.ExecuteReader();
            List<Pokemon> pokemons = new List<Pokemon> { };
            while (pokemon.Read())
            {
                pokemons.Add(new Pokemon(int.Parse(pokemon.GetString(0)), pokemon.GetString(1).Trim(), int.Parse(pokemon.GetString(2)), pokemon.GetString(3).Trim(), pokemon.GetString(4).Trim(), int.Parse(pokemon.GetString(5)) == 1));
            }
            pokemons.ForEach(poke => Console.WriteLine(poke));
        }

        // Upadate a pokemon
        public static void UpadatePokemon(NpgsqlConnection con,string referenciaValor, string referenciaAtrivuto, string atrivuto, string newValor)
        {
            NpgsqlCommand cmd = new NpgsqlCommand($"UPDATE pokemon SET {atrivuto} = '{newValor}' WHERE {referenciaAtrivuto} = '{referenciaValor}'", con);
            cmd.ExecuteNonQuery();
        }

        // Shows all pokemon on the pokemon table
        private static void SelectPokemon(NpgsqlConnection con, string tableName)
        {
            string sql = $"SELECT * FROM {tableName}";
            using var cmd = new NpgsqlCommand(sql, con);
            using NpgsqlDataReader rdr = cmd.ExecuteReader();

            // Shows all pokemons via console.
            while (rdr.Read())
            {
                string pokemonName = rdr["name"].ToString();
                Console.WriteLine(pokemonName);
            }
        }

        // Loads the .csv file, cleans it up and adds the values to the SQL table.
        private static void LoadFile(NpgsqlConnection con, string tableName, List<string> stats)
        {
            // File path.
            string filePath = @"..\..\..\pokemon.csv";

            // Check if the CSV file exists.
            if (!File.Exists(filePath))
            {
                // Message stating CSV file could not be located.
                Console.WriteLine("Could not locate the CSV file.");
                return;
            }

            // Assign the CSV file to a reader object.
            StreamReader reader = new StreamReader(filePath);
            string[] currentRow;
            string line;

            // Cleans the .CSV file.
            while ((line = reader.ReadLine()) != null)
            {
                line = line.Replace("['", "").Replace("']", "");
                currentRow = line.Split(';');
                if (line.Contains(";name")) continue;
                InsertDataPokemon(currentRow, con, tableName, stats);
            }
        }

        // Creates the pokemon table.
        private static void CreateTable(NpgsqlConnection con, string tableName, List<string> stats)
        {
            // Creates the SQL table
            string sql = $"DROP TABLE IF EXISTS {tableName}; CREATE TABLE {tableName} ({string.Join(" CHAR(250), ", stats.ToArray())} CHAR (250))";

            // Sends the create query.
            NpgsqlCommand commmand = new NpgsqlCommand(sql, con);
            commmand.ExecuteNonQuery();
        }

        // Inserts all the values into the pokemon table.
        private static void InsertDataPokemon(string[] pokemonRow, NpgsqlConnection con, string tableName, List<string> stats)
        {
            // Opens a connection and inserts pokemon data.
            string sql = $"INSERT INTO {tableName}({string.Join(",", stats.ToArray())}) VALUES(@{string.Join(",@", stats.ToArray())})";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

            // Adds the values and prepares them.
            for (int i = 0; i < stats.Count; i++) { cmd.Parameters.AddWithValue(stats[i], pokemonRow[i]); }
            cmd.Prepare();

            // Executes the command and closes the connection.
            cmd.ExecuteNonQuery();
        }

        // Checks the connection state, and returns true/false if open.
        private static bool CheckStateConnection(NpgsqlConnection con)
        {
            if (con.State == System.Data.ConnectionState.Open)
            {
                Console.WriteLine("Conectado");
                return true;
            }
            return false;
        }

        // Creates a connection and returns it.
        private static NpgsqlConnection Connection(string dbName)
        {
            string cs = $"Host=localhost;Username=postgres;Password=postgres;Database={dbName}";
            NpgsqlConnection con = new NpgsqlConnection(cs);
            return con;
        }
    }
}